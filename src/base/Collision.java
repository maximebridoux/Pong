package base;

import base.display.Ball;
import base.display.Field;
import base.display.obstacle.Obstacle;
import data.Point;
import data.Segment;
import data.Vector;

import java.util.Collections;

/**
 * Computes the next collision between a ball and obstacles in a map.
 */
public class Collision
{
    /**
     * The coordinates of the collision.
     */
    private Point _coordinates = new Point();

    /**
     * The distance to the collision.
     */
    private double _distance = 0;

    /**
     * The segment that will be hit.
     */
    private Segment _segment = new Segment();

    /**
     * The obstacle that will be hit.
     */
    private Obstacle _obstacle;

    public Collision()
    {
        _coordinates = new Point();
        _distance = 0;
        _segment = new Segment();
        _obstacle = new Obstacle() {
            @Override
            public void createShape() {}
        };
    }

    public Collision(Ball ball, Field map)
    {
        computeCollision(ball, map);
        checkComputedCollision(ball, map);
    }

    /**
     * Computes the next collision based on current information if the whole map was static.
     * @param ball the ball
     * @param map the map
     */
    private void computeCollision(Ball ball, Field map)
    {
        java.util.Vector<Point> collisions = new java.util.Vector<>();
        java.util.Vector<Double> distances = new java.util.Vector<>();
        java.util.Vector<Segment> segments = new java.util.Vector<>();
        java.util.Vector<Obstacle> obstacles = new java.util.Vector<>();


        for (int i = 0; i < map.getObstacles().size(); i++)
        {
            for (int j = 0; j < map.getObstacles().elementAt(i).getSegments().size(); j++)
            {
                Segment terrainLocation = map.getObstacles().elementAt(i).getSegments().elementAt(j);
                double[] k = solveSystem(ball.getPosition(),ball.getDirection(), terrainLocation);
                if (possibleCollision(k, ball.getRadius(), ball.getPosition(), ball.getDirection(), terrainLocation))
                {
                    collisions.addElement(collisionWithSegment(k, ball.getRadius(), ball.getPosition(), ball.getDirection(), terrainLocation));
                    distances.addElement(Point.euclideanDist(collisions.lastElement(), ball.getPosition()));
                    segments.addElement(terrainLocation);
                    obstacles.addElement(map.getObstacles().elementAt(i));
                }
            }
        }
        assert(collisions.size() > 0);
        assert(distances.size() > 0);
        int index = distances.indexOf(Collections.min(distances));

        _coordinates = collisions.elementAt(index);
        _distance = distances.elementAt(index);
        _segment = segments.elementAt(index);
        _obstacle = obstacles.elementAt(index);
    }

    /**
     * Computes the solution of a system giving the intersection point between
     * a point plus a direction vector and a segment both considered as lines.
     * @param p the point
     * @param v the direction vector for the point
     * @param s the static segment to be hit
     * @return the system solution
     */
    private static double[] solveSystem(Point p, Vector v, Segment s)
    {
        assert(p != null);
        assert(v!= null);
        assert(s != null);

        // Solution of the following system:
        // a k1 + b k2 = c
        // d k1 + e k2 = f

        double nextX = p.getX()+v.getDirX();
        double nextY = p.getY()+v.getDirY();

        double a = v.getDirX();                 //x2  - x1
        double b = s.getX1() - s.getX2();       //x'1 - x'2
        double c = s.getX1() - p.getX();        //x'1 - x1
        double d = v.getDirY();                 //y2  - y1
        double e = s.getY1() - s.getY2();       //y'1 - y'2
        double f = s.getY1() - p.getY();        //y'1 - y1

        double systemDet = a*e - b*d;

        if (systemDet == 0)
        {
            return new double[] {0, 0, 0};
        }
        else
        {
            double firstDet = c*e-b*f;
            double secondDet = a*f-c*d;
            return new double[] {systemDet, firstDet/systemDet,  secondDet/systemDet};
        }
    }

    /**
     * Computes the collision between a point plus a direction vector and a segment.
     * @param k the system's solution
     * @param radius the radius of the point
     * @param p the point
     * @param v the direction's vector
     * @param s the segment
     * @return the collision point
     */
    private static Point collisionWithSegment(double[] k, double radius, Point p, Vector v, Segment s)
    {
        assert(k[0] != 0);
        double distX = k[1]*v.getDirX();
        double distY = k[1]*v.getDirY();

        double CollX = 0;
        double CollY = 0;

        if (v.getDirX() == 0)
        {
            CollX = distX;
            CollY = removeRadius(distY, radius);
        }
        else if (v.getDirY() == 0)
        {
            CollY = distY;
            CollX = removeRadius(distX, radius);
        }

        if (Math.abs(distX) >= Math.abs(distY))
        {
            CollX = removeRadius(distX, radius);
            CollY = CollX*v.getDirY()/v.getDirX();
        }
        else
        {
            CollY = removeRadius(distY, radius);
            CollX = CollY*v.getDirX()/v.getDirY();
        }
        return new Point(p.getX() + CollX, p.getY() + CollY);
    }

    /**
     * Removes the radius' influence from a distance.
     * @param dist the distance
     * @param radius the radius
     * @return the distance without the radius
     */
    public static double removeRadius(double dist, double radius)
    {
        if (dist > 0)
            return dist - radius;
        else
            return dist + radius;
    }

    /**
     * Checks if a collision is possible between a point plus a direction vector and a segment.
     * @param k the system's solution
     * @param radius the radius of the point
     * @param p the point
     * @param v the direction vector
     * @param s the segment
     * @return true iif a collision is possible
     */
    public static boolean possibleCollision(double[] k, double radius, Point p, Vector v, Segment s)
    {
        if (k[0] == 0) //systemDet = 0 : parallel lines (lines are collinear)
        {
            return false;
        }
        else
        {
            if (k[1] >= 0 && k[2] >= 0)
            {
                double uncorrectedCollisionX = p.getX()+ k[1]*v.getDirX();
                double uncorrectedCollisionY = p.getY()+ k[1]*v.getDirY();
                Point uncorrectedP = new Point(uncorrectedCollisionX, uncorrectedCollisionY);
                Point correctedP = collisionWithSegment(k, radius, p, v, s);

                return (uncorrectedP.isProjInSegment(s) && correctedP.isProjInSegment(s));
            }
            else
            {
                return false;
            }
        }
    }

    /**
     * Checks if two segments cross.
     * @param s1 the first segment
     * @param s2 the segment segment
     * @return true iif the segments cross
     */
    public static boolean cross(Segment s1, Segment s2)
    {
        Point p = s1.getP1();
        Vector v = new Vector(s1.getX2()-s1.getX1(), s1.getY2()-s1.getY1());
        double[] k = solveSystem(p, v, s2);
        double uncorrectedCollisionX = p.getX()+ k[1]*v.getDirX();
        double uncorrectedCollisionY = p.getY()+ k[1]*v.getDirY();
        Point uncorrectedP = new Point(uncorrectedCollisionX, uncorrectedCollisionY);
        return (k[0] != 0 && k[1] >= 0 && k[2] >=0
                && uncorrectedP.isProjInSegment(s1) && uncorrectedP.isProjInSegment(s1.reverseS())
                && uncorrectedP.isProjInSegment(s2) && uncorrectedP.isProjInSegment(s2.reverseS()));
    }

    /**
     * Gives the intersection point of two segments if they cross.
     * @param s1 the first segment
     * @param s2 the second segment
     * @return the intersection point
     */
    private static Point intersection(Segment s1, Segment s2)
    {
        Point p = s1.getP1();
        Vector v = new Vector(s1.getX2()-s1.getX1(), s1.getY2()-s1.getY1());
        double[] k = solveSystem(p, v, s2);
        assert(k[0] != 0);
        double uncorrectedCollisionX = p.getX()+ k[1]*v.getDirX();
        double uncorrectedCollisionY = p.getY()+ k[1]*v.getDirY();
        return new Point(uncorrectedCollisionX, uncorrectedCollisionY);
    }

    /**
     * Checks if the computed collision is still valid
     * @param ball the ball
     * @param map the map
     */
    public void checkComputedCollision(Ball ball, Field map)
    {
        Segment path = new Segment(ball.getPosition(),_coordinates);


        java.util.Vector<Segment> intersected = new java.util.Vector<>();
        java.util.Vector<Double> distances = new java.util.Vector<>();
        java.util.Vector<Point> collisions = new java.util.Vector<>();
        java.util.Vector<Obstacle> obstacles = new java.util.Vector<>();


        for (int i = 0; i<map.getObstacles().size(); i++)
        {
            for (int j = 0; j < map.getObstacles().elementAt(i).getSegments().size(); j++)
            {
                Segment terrainLocation = map.getObstacles().elementAt(i).getSegments().elementAt(j);
                if (cross(path, terrainLocation) ||
                        cross(path.translateS(ball.getRadius()+1), terrainLocation) ||
                        cross(path.translateS(-ball.getRadius()-1), terrainLocation))
                {
                    /**
                     System.out.println(path.getX1());
                     System.out.println(path.getY1());
                     System.out.println(path.getX2());
                     System.out.println(path.getY2());
                     System.out.println(" ");
                     System.out.println(terrainLocation.getX1());
                     System.out.println(terrainLocation.getY1());
                     System.out.println(terrainLocation.getX2());
                     System.out.println(terrainLocation.getY2());*/
                    System.out.println("----------------");
                    Point collision = intersection(terrainLocation, path);
                    collisions.addElement(collision);
                    intersected.addElement(terrainLocation);
                    distances.addElement(Point.euclideanDist(ball.getPosition(), collision));
                    obstacles.addElement(map.getObstacles().elementAt(i));
                }
            }
        }
        if (intersected.size() > 0) //a segment was forgotten, need for a more precise collision detection based on radius
        {
            assert(collisions.size() > 0);
            assert(distances.size() > 0);
            int index = distances.indexOf(Collections.min(distances));
            _coordinates = correctCoordinates(ball, collisions.elementAt(index));
            _distance = distances.elementAt(index) - 50;
            _segment = intersected.elementAt(index);
            _obstacle = obstacles.elementAt(index);
        }
    }

    /**
     * Computes the coordinates of the ball in case of collision on p based on the checking method
     * @param ball the ball
     * @param p the collision impact
     * @return the coordinates of the ball when collision occur
     */
    public static Point correctCoordinates(Ball ball, Point p)
    {
        double factor = ball.getRadius()/Math.sqrt(Math.pow(ball.getDirection().getDirX(), 2) + Math.pow(ball.getDirection().getDirY(), 2));
        return new Point(p.getX() - factor*ball.getDirection().getDirX(), p.getY() - factor*ball.getDirection().getDirY());
    }

    public Point getCoodinates()
    {
        return _coordinates;
    }

    public double getDistance()
    {
        return _distance;
    }

    public Segment getSegment()
    {
        return _segment;
    }

    public Obstacle getObstacle() {
        return _obstacle;
    }
}
