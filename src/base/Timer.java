package base;

import base.display.Ball;
import base.display.Field;
import base.display.racket.AbstractRacket;
import base.display.racket.IARacket;
import javafx.animation.AnimationTimer;
import javafx.animation.Timeline;

public class Timer extends AnimationTimer {
    private long startTime = -1;
    private long _time;
    private Field _map;

    public Timer(Field map)
    {
        _map = map;
    }

    @Override
    public void handle(long timestamp)
    {
        if (startTime==-1)
            startTime = timestamp;

        long totalElapsedNanoseconds = timestamp - startTime;
        long n = totalElapsedNanoseconds/(1000000000);
        _time = totalElapsedNanoseconds/1000000;

        if (_time%2000 == 0)
        {
            for (int i =0; i < _map.getIArackets().size(); i++)
            {
                IARacket racket = _map.getIArackets().elementAt(i);
                racket.computeNextPosition();
            }
        }
    }

    public long getElapsedTime()
    {
        return _time;
    }
}
