package base;

/**
 * New exception to throw to signal a winning player.
 */
public class WinException extends Exception
{
    private int _winningPlayer = -1;

    public WinException(int i)
    {
        _winningPlayer = i;
    }

    public int getWinningPlayer()
    {
        return _winningPlayer;
    }
}
