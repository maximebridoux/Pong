package base.display.obstacle;

import data.Point;
import data.Segment;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

/**
 * Base obstacle.
 */
public class Wall extends StaticObstacle
{
    private Segment _segment;
    private boolean _winningWall = false;
    private int _winningPlayer = -1;

    public Wall()
    {
        new Wall(0,0,0,0);
    }

    public Wall(double x1, double y1, double x2, double y2)
    {
        new Wall(new Point(x1, y1), new Point(x2, y2));
    }

    public Wall(Point p1, Point p2)
    {
        _segment = new Segment(p1, p2);
        addSegment(_segment);
        createShape();
    }

    /**
     * Creates a black line based shape.
     */
    @Override
    public void createShape()
    {
        Line line = new Line();
        line.setStartX(_segment.getX1());
        line.setStartY(_segment.getY1());
        line.setEndX(_segment.getX2());
        line.setEndY(_segment.getY2());
        line.setFill(Color.BLACK);
        _shape = line;
    }

    /**
     * Marks the wall as winning for a player.
     * @param player the player that will win a point
     */
    public void setWinning(int player)
    {
        assert(player >= 0);
        _winningWall = true;
        _winningPlayer = player;
    }

    /**
     * Marks the wall as ordinary.
     */
    public void setLosing()
    {
        _winningWall = false;
        _winningPlayer = -1;
    }

    @Override
    public boolean isWinningWall()
    {
        return _winningWall;
    }

    @Override
    public int getWinningPlayer()
    {
        return _winningPlayer;
    }
}
