package base.display.maps;

import base.display.Field;
import data.Point;
import javafx.scene.Scene;
import javafx.scene.paint.Color;

/**
 * Map made of 3 rectangles with 4 walls.
 * Designed for collision testing.
 */
public class WallTest extends Field
{
    public WallTest()
    {
        new WallTest(800,600);
    }

    public WallTest(double width, double height)
    {
        _scene = new Scene(_root, width, height);
        _scene.setFill(Color.WHITE);
        Point p1 = new Point(0,0);
        Point p2 = new Point(800,600);
        generateWallRect(p1, p2);

        Point p3 = new Point(200,0);
        Point p4 = new Point(400,200);
        generateWallRect(p3, p4);

        Point p5 = new Point(600, 200);
        Point p6 = new Point(600, 500);
        generateWallRect(p5, p6);

    }
}
