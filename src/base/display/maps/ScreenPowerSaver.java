package base.display.maps;

import base.display.Field;
import base.display.Score;
import base.display.obstacle.Wall;
import data.Point;
import javafx.scene.Scene;
import javafx.scene.paint.Color;

/**
 * Map made of a rectangle with 4 walls.
 * Designed for classic pong game.
 */
public class ScreenPowerSaver extends Field
{
    public ScreenPowerSaver()
    {
        new ScreenPowerSaver(800,600);
    }

    public ScreenPowerSaver(double width, double height)
    {
        _scene = new Scene(_root, width, height);
        _scene.setFill(Color.WHITE);

        _playerNumber = 2;

        generateWallRect(new Point(0,0), new Point(width, height));
        ((Wall) _obstacle.get(_obstacle.size()-1)).setWinning(1);
        ((Wall) _obstacle.get(_obstacle.size()-3)).setWinning(0);

        _score = new Score(_playerNumber, 3);
        addScore();
    }
}
