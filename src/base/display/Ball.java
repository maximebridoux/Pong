package base.display;

import base.Engine;
import base.Timer;
import base.display.obstacle.Obstacle;
import data.Point;
import data.Segment;
import data.Vector;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 * Creates a ball to play with.
 */
public class Ball
{
    /**
     * The center of the ball.
     */
    private Point _location;

    /**
     * The direction of the ball.
     */
    private Vector _direction;

    /**
     * The radius of the ball.
     */
    private double _radius;

    /**
     * The color of the ball.
     */
    private Color _color;

    /**
     * The shape of the ball.
     */
    private Circle _circle;

    /**
     * The next hit obstacle.
     */
    private Obstacle _currentObstacle = new Obstacle() {
        @Override
        public void createShape() {
        }
    };

    public Ball()
    {
        _location = new Point(0,0);
        _direction = new Vector(0,0);
        _color = Color.BLACK;

        _circle = new Circle();
        _circle.setRadius(0);
        _circle.setCenterX(0);
        _circle.setCenterY(0);
        _circle.setFill(_color);
    }

    public Ball(double x, double y, double dirX, double dirY, double radius, Color color)
    {
        _location = new Point(x,y);
        _direction = new Vector(dirX,dirY);
        _radius = radius;
        _color = color;

        _circle = new Circle();
        _circle.setRadius(radius);
        _circle.setCenterX(x-radius);
        _circle.setCenterY(y-radius);
        _circle.setFill(color);
    }

    public double getRadius()
    {
        return _radius;
    }

    public Circle getCircle()
    {
        return _circle;
    }

    public Point getPosition()
    {
        return _location;
    }

    public Vector getDirection()
    {
        return _direction;
    }

    public double getX()
    {
        return getPosition().getX();
    }

    public double getY()
    {
        return getPosition().getY();
    }

    public double getDirX()
    {
        return getDirection().getDirX();
    }

    public double getDirY()
    {
        return getDirection().getDirY();
    }

    public void setLocation(Point p)
    {
        _location = p;
    }

    public void setDirection(Vector v)
    {
        _direction = v;
    }

    public Obstacle getCurrentObstacle() {
        return _currentObstacle;
    }

    public void setCurrentObstacle(Obstacle currentObstacle) {
        _currentObstacle = currentObstacle;
    }
}
