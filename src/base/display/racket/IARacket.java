package base.display.racket;

import data.Point;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.util.Duration;

/**
 * Creates an IA controlled racket.
 */
public class IARacket extends AbstractRacket
{
    private boolean b = true;
    public IARacket(Point center, double width, double height, double min, double max)
    {
        _IA = true;
        _center = center;
        _width = width;
        _height = height;
        setUpRacket();
        _timeline.setOnFinished((ActionEvent event) -> {
            //computeNextPosition();
        });
        _timeline.getKeyFrames().clear();
        _timeline.getKeyFrames().add(
                new KeyFrame(Duration.ZERO, new KeyValue(getShape().translateXProperty(), 0))
        );
    }

    public void setUpControls(Scene scene) {}

    /**
     * Moves the IA to its next position
     */
    public void computeNextPosition()
    {
        moveTo(new Point(_center.getX(), 400));
    }
}
