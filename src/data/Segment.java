package data;

import base.display.Ball;

/**
 * Represents a segment of two points.
 */
public class Segment
{
    private Point _p1;
    private Point _p2;

    public Segment()
    {
        _p1 = new Point(0,0);
        _p2 = new Point(0,0);
    }

    public Segment(double x1, double y1, double x2, double y2)
    {
        _p1 = new Point(x1, y1);
        _p2 = new Point(x2, y2);
    }

    public Segment(Point p1, Point p2)
    {
        _p1 = p1;
        _p2 = p2;
    }

    public double getX1()
    {
        return _p1.getX();
    }
    public double getY1()
    {
        return _p1.getY();
    }
    public double getX2()
    {
        return _p2.getX();
    }
    public double getY2()
    {
        return _p2.getY();
    }
    public Point getP1()
    {
        return _p1;
    }
    public Point getP2()
    {
        return _p2;
    }

    /**
     * Computes the reversed segment
     * @return the reversed segment
     */
    public Segment reverseS()
    {
        return new Segment(getP2(), getP1());
    }

    /**
     * Translates a segment to the right
     * @param r the direction to translate
     * @return the translated segment
     */
    public Segment translateS(double r)
    {
        double y = Math.pow(getY2()-getY1(), 2);
        double x = Math.pow(getX2()-getX1(), 2);
        double deltaX = r*(getY2()-getY1())/Math.sqrt(x+y);
        double deltaY = r*(getX2()-getX1())/Math.sqrt(x+y);
        return new Segment(getX1()+deltaX, getY1()+deltaY, getX2()+deltaX, getY2()+deltaY);
    }
}
