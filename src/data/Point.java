package data;

/**
 * Represents a point of two coordinates.
 */
public class Point
{
    private double _x;
    private double _y;

    public Point()
    {
        _x = 0;
        _y = 0;
    }

    public Point(double x, double y)
    {
        _x = x;
        _y = y;
    }

    public double getX()
    {
        return _x;
    }

    public double getY()
    {
        return _y;
    }

    public static double euclideanDist(Point p1, Point p2)
    {
        return Math.sqrt(Math.pow(p1.getX() - p2.getX(), 2) + Math.pow(p1.getY() - p2.getY(), 2));
    }

    public double projOnSegment(Segment s)
    {
        double v1x = getX() - s.getX1();
        double v1y = getY() - s.getY1();
        double s1x = s.getX2() - s.getX1();
        double s1y = s.getY2() - s.getY1();
        return v1x*s1x + v1y*s1y;
    }

    public boolean isProjInSegment(Segment s)
    {
        return ((projOnSegment(s) >= 0) && (projOnSegment(s.reverseS()) >= 0));
    }

    public Point add(Point p)
    {
        return new Point(getX() + p.getX(), getY() + p.getY());
    }

    public Point multiply(double d)
    {
        return new Point(getX()*d, getY()*d);
    }

}
